set notitle
set bg=dark
set go=a
set mouse=r
set nohlsearch
set clipboard=unnamed
set noshowmode
set ruler
set laststatus=0
set noshowcmd
set encoding=utf-8
set shiftwidth=4
set tabstop=4
set shiftwidth=4
"set expandtab
set ignorecase
set smartcase
set number

nnoremap c "_c
filetype plugin on
syntax on
