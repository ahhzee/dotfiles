" Vim syntax file
" Language:		4chan/nel
" Maintainer:	aussir
" Last change: 2021 Jan 27t

if exists("b:current_syntax")
  finish
endif

syn match grnTxt    '^>.*'
syn match fwrd      '^>>[0-9][0-9][0-9][0-9].*'
syn match desc      '^---.*'
syn match lnk       '^https://.*'
syn match lnk       '^http://.*'

let b:current_syntax = "chan"
hi def link grnTxt  Comment
hi def link fwrd    PreProc
hi def link desc    Statement
hi def link lnk     Constant
