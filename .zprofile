#!/bin/zsh

# ~/.local/bin in path
export PATH="$(du "$HOME/.local/bin" | cut -f2 | paste -sd ':' -):$PATH"

# default programs
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="qutebrowser"
export SUDO_ASKPASS="/usr/lib/ssh/x11-ssh-askpass"

# cleaner history
export HISTCONTROL=ignoredups:erasedups

# set language
export LC_ALL="en_US.UTF-8"

# who knows?
export KEYTIMEOUT=1

# less settings
export PAGER="/usr/bin/less"
export LESS="-F -r -W -x4 -z-4 -X --prompt=%f - %lt"
export MANLESS="\$MAN_PN\ \-\ %lt"
# White
export LESS_TERMCAP_mb=$'\E[1;49m'     # begin blink
export LESS_TERMCAP_md=$'\E[1;97m'     # begin bold
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_so=$'\E[1;37m'     # begin reverse video
export LESS_TERMCAP_us=$'\E[4;97m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

# cleaning up stuff
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export BTPD_HOME="$XDG_CONFIG_HOME/btpd"
export LESSHISTFILE="-"
export IPFS_PATH="$XDG_DATA_HOME/ipfs"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export HISTFILE="$XDG_DATA_HOME/history"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
export GOPATH="$XDG_DATA_HOME/go"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export XINITRC="$XDG_CONFIG_HOME/x11/xinitrc"

# for man
export MANPAGER="less"

# mpd stuff
export MPD_HOST="localhost"
export MPD_PORT="6789"

# Auto startx on login
[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && clear && exec startx "~/.config/x11/xinitrc" > ~/.local/share/xorg/startx.log
